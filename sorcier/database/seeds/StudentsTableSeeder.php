<?php


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Students;
use App\Programs;
use App\Proffesors;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $françois = Students::create([
            'first_name' => 'François',
            'second_name' => 'Troulic',
            'adress' => '1256 Allée des tullipes',
            'birthday' => '1999/09/23',
            'inscription_date' => '2018/09/15',
            'programs_id' => '1',
        ]);
        $program = Programs::find(1);
        $françois->programs()->associate($program);
        $françois->proffesors()->attach(1);
        $françois->curses()->attach([1, 2, 3]);
        $françois->save();

        $bernard = Students::create([
            'first_name' => 'Bernard',
            'second_name' => 'Carpe',
            'adress' => '1478 Rue Potter',
            'birthday' => '1996/10/13',
            'inscription_date' => '2016/09/15',
            'programs_id' => '2',
        ]);
        $program = Programs::find(3);
        $bernard->programs()->associate($program);
        $bernard->proffesors()->attach(3);
        $bernard->curses()->attach(3);
        $bernard->save();


        $elliel = Students::create([
            'first_name' => 'Elliel',
            'second_name' => 'Santa',
            'adress' => '40 Avenue du Bosquet',
            'birthday' => '1998/11/09',
            'inscription_date' => '2017/09/15',
            'programs_id' => '3',
        ]);
        $program = Programs::find(2);
        $elliel->programs()->associate($program);
        $elliel->proffesors()->attach(2);
        $elliel->curses()->attach(2);
        $elliel->save();


        $damien = Students::create([
            'first_name' => 'Damien',
            'second_name' => 'Silverfang',
            'adress' => '58 Route des Piverts',
            'birthday' => '1999/06/10',
            'inscription_date' => '2019/09/15',
            'programs_id' => '4',
        ]);
        $program = Programs::find(1);
        $damien->programs()->associate($program);
        $damien->proffesors()->attach(1);
        $damien->curses()->attach(1);
        $damien->save();


        $winston = Students::create([
            'first_name' => 'Winston',
            'second_name' => 'Soulheater',
            'adress' => 'Allée royale Jean-Marc',
            'birthday' => '1994/02/20',
            'inscription_date' => '2016/09/15',
            'programs_id' => '5',
        ]);
        $program = Programs::find(5);
        $winston->programs()->associate($program);
        $winston->proffesors()->attach(5);
        $winston->curses()->attach(5);
        $winston->save();


        $luden = Students::create([
            'first_name' => 'Luden',
            'second_name' => 'Michigan',
            'adress' => '345 Road of Houstering',
            'birthday' => '1997/01/18',
            'inscription_date' => '2018/09/15',
            'programs_id' => '1',
        ]);
        $program = Programs::find(1);
        $luden->programs()->associate($program);
        $luden->proffesors()->attach(4);
        $luden->curses()->attach(4);
        $luden->save();
    }
}
