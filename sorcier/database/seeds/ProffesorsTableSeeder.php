<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProffesorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('proffesors')->insert([
            'first_name' => 'Anabelle',
            'second_name' => 'Sister',
            'email' => 'sisteranabelle@gmail.com',
            'type_of_status' => 'salarié',
        ]);

        DB::table('proffesors')->insert([
            'first_name' => 'Francis',
            'second_name' => 'Register',
            'email' => 'registerfrancis@gmail.com',
            'type_of_status' => 'salarié',
        ]);

        DB::table('proffesors')->insert([
            'first_name' => 'Bodrigue',
            'second_name' => 'Lorem',
            'email' => 'lorembodrigue@gmail.com',
            'type_of_status' => 'prestatère',
        ]);

        DB::table('proffesors')->insert([
            'first_name' => 'Foulius',
            'second_name' => 'Noriger',
            'email' => 'norigerfoulius@gmail.com',
            'type_of_status' => 'prestatère',
        ]);

        DB::table('proffesors')->insert([
            'first_name' => 'Catherine',
            'second_name' => 'Laiton',
            'email' => 'laitoncatherine@gmail.com',
            'type_of_status' => 'salarié',
        ]);
    }
}

