<?php

use App\Http\Resources\Programs;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('curses')->insert([

            'name' => 'Maitrise du balait',
            'duration' => '4h',
            'programs_id'=>'1',
        ]);

//________________________________________________

        DB::table('curses')->insert([
            'name' => 'Sorts basiques',
            'duration' => '2h30',
            'programs_id'=>'2',
        ]);

        DB::table('curses')->insert([
            'name' => 'Sorts intermediaire',
            'duration' => '2h30',
            'programs_id'=>'3',
        ]);

        DB::table('curses')->insert([
            'name' => 'Sorts expert',
            'duration' => '2h30',
            'programs_id'=>'4',
        ]);

//________________________________________________

        DB::table('curses')->insert([
            'name' => 'Potions 1er niveau',
            'duration' => '3h',
            'programs_id'=>'5',
        ]);

        DB::table('curses')->insert([
            'name' => 'Potions 2nd niveau',
            'duration' => '3h',
            'programs_id'=>'1',
        ]);

        DB::table('curses')->insert([
            'name' => 'Potions 3eme niveau',
            'duration' => '3h',
            'programs_id'=>'2',
        ]);

        DB::table('curses')->insert([
            'name' => 'Potions 4eme niveau',
            'duration' => '3h',
            'programs_id'=>'3',
        ]);

        DB::table('curses')->insert([
            'name' => 'Potions 5eme niveau',
            'duration' => '3h',
            'programs_id'=>'4',
        ]);

//________________________________________________

        DB::table('curses')->insert([
            'name' => 'Deffense contre les forces du mal',
            'duration' => '1h30',
            'programs_id'=>'5',
        ]);

//________________________________________________

        DB::table('curses')->insert([
            'name' => 'Animaux magiques',
            'duration' => '4h',
            'programs_id'=>'1',
        ]);
    }
}
