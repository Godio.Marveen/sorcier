<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('programs')->insert([
            'name' => 'Magie blanche 1ere Année',
            'curses_includes' => 'Sorts basiques, Potions 1er niveau, Maitrise du balait',
        ]);

        DB::table('programs')->insert([
            'name' => 'Magie blanche 2eme Année',
            'curses_includes' => 'Sorts intermediaires, Potions 2eme niveau, Deffense contre les forces du mal',
        ]);

        DB::table('programs')->insert([
            'name' => 'Magie blanche 3eme Année',
            'curses_includes' => 'Sorts intermédiaires, Potions 3eme niveau, Deffense contre les forces du mal',
        ]);

        DB::table('programs')->insert([
            'name' => 'Magie blanche 4eme Année',
            'curses_includes' => 'Sorts expert, Potions 4eme niveau, Deffense contre les forces du mal, Animaux magiques',
        ]);

        DB::table('programs')->insert([
            'name' => 'Magie blanche 5eme Année',
            'curses_includes' => 'Sorts expert, Potions 5eme niveau, Deffense contre les forces du mal, Animaux magiques',
        ]);
    }
}
