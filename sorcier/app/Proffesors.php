<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proffesors extends Model
{
    public function students()
    {
        return $this->belongsToMany('App\Students','students_has_proffesors');
    }

    public function curses()
    {
        return $this->belongsToMany('App\Curses','proffesors_has_curses');
    }
}
