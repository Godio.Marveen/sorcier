<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programs extends Model
{
    public function students()
    {
        return $this->hasMany('App\Students');
    }

    public function curses()
    {
        return $this->hasMany('App\Curses');
    }
}
