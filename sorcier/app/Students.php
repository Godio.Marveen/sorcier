<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    public function programs()
    {
        return $this->belongsTo('App\Programs');
    }

    public function curses()
    {
        return $this->belongsToMany('App\Curses', 'students_has_curses');
    }

    public function proffesors()
    {
        return $this->belongsToMany('App\Proffesors', 'students_has_proffesors');
    }


}

