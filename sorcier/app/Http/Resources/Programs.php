<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Programs extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'Nom du programme' => $this->name,
            'Cours inclus' => $this->curses_includes,
        ];
    }
}
