<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Programs as ProgramsResource;
use App\Http\Resources\Proffesors as ProffesorsResource;
use App\Http\Resources\Curses as CursesResource;
use App\Programs;
use App\Proffesors;
class Students extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'Prénom :' => $this->first_name,
            'Nom :' => $this->second_name,
            'Adresse :' => $this->adress,
            'Date de naissance :' => $this->birthday,
            'inscrit le :' => $this->inscription_date,

            'programme' => new ProgramsResource(Programs::find($this->programs_id)),
            'Professeurs' => ProffesorsResource::collection($this->proffesors),
            'Cours' => CursesResource::collection($this->curses),
        ];
    }
}
