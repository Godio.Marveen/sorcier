<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curses extends Model
{
    public function students()
    {
        return $this->belongsToMany('App\Students','students_has_curses');
    }

    public function programs()
    {
        return $this->belongsTo('App\Programs');
    }

    public function proffesors()
    {
        return $this->belongsToMany('App\Proffesors','proffesors_has_curses');
    }
}
