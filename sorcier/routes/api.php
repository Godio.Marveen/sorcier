<?php

use Illuminate\Http\Request;
use App\Students;
use App\Programs;
use App\Curses;
use App\Http\Resources\Students as StudentsResource;
use App\Http\Resources\Programs as ProgramsResource;
use App\Http\Resources\Proffesors as ProffesorsResource;
use App\Http\Resources\Curses as CursesResource;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/students', function () {
    return StudentsResource::collection(Students::all());
});

Route::get('/programs', function () {
    return ProgramsResource::collection(Programs::all());
});

Route::get('/curses', function () {
    return CursesResource::collection(Curses::all());
});

Route::get('/proffesors', function () {
    return ProffesorsResource::collection(Programs::all());
});

Route::get('students/{id}', function ($id) {
    return new StudentsResource(Students::find($id));
});

Route::get('programs/{id}', function ($id) {
    return new ProgramsResource(Programs::find($id));
});

Route::get('curses/{id}', function ($id) {
    return new CursesResource(Curses::find($id));
});

Route::get('proffesors/{id}', function ($id) {
    return new ProffesorsResource(Curses::find($id));
});
