# sorcier

Présentation de mon projet :

1 -> Pull le projet de git sur votre Ordinateur.

2 -> Ouvrir votre terminal et aller sur votre projet.

3 -> Faire un : php artisan serve et aller sur le lien affiché 

4 -> une fois sur le lien dans la barre de navigation taper : 
        http://127.0.0.1:8000/api/students
Pour voir la liste de tous les etudiants ave leurs descriptions .

5 -> Vous pouvez aussi choisir un etudiant en particulier grace à son id ex :
        http://127.0.0.1:8000/api/students/1

ça vous donnera les informations de l'élève 1 en particulier.

6 -> Idem pour voir les programmes : 

        http://127.0.0.1:8000/api/programs

7 -> Pour voir un programme en particulier: 

http://127.0.0.1:8000/api/programs/1